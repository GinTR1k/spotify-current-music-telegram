# Spotify current music in Telegram


# Installation
See `spotify_current_music_telegram/config.py`


## Translations
### Create new translation:
```bash
pybabel init -i translations/base.pot -d translations -l HERE_PASS_LANG_CODE
```

Example:
```bash
pybabel init -i translations/base.pot -d translations -l ru
```
