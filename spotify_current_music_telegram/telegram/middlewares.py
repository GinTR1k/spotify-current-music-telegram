from aiogram import types
from aiogram.dispatcher.handler import current_handler, CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware

from spotify_current_music_telegram.models import User
from spotify_current_music_telegram.telegram.locale import _


class GetUserMiddleware(BaseMiddleware):
    async def on_process_message(self, message: types.Message, data: dict):
        handler = current_handler.get()
        user = await User.query.where(
            User.telegram_user_id == message.from_user.id).gino.first()
        print(handler, getattr(handler, 'auth_not_required', False), user)

        if (
                handler
                and not getattr(handler, 'auth_not_required', False)
                and not user
        ):
            await message.reply(_(
                'Sorry, you need to agree terms of use to use this command. '
                'Send /start to continue.'))
            raise CancelHandler()

        data['user'] = user
