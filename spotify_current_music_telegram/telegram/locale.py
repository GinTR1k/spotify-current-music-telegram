from aiogram.contrib.middlewares.i18n import I18nMiddleware

# Locale
i18n = I18nMiddleware('messages')

# Alias for gettext method
_ = i18n.gettext
# Alias for gettext method, parser will understand double underscore as
# plural (aka ngettext)
__ = i18n.gettext
