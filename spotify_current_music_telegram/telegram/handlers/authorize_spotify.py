from aiogram import types

from spotify_current_music_telegram.config import Config
from spotify_current_music_telegram.models import User
from spotify_current_music_telegram.telegram import dispatcher
from spotify_current_music_telegram.telegram.locale import _
from spotify_current_music_telegram.utils import auth_not_required


@dispatcher.message_handler(commands=['authorize'])
async def agree_terms(message: types.Message):
    """This handler will be called when user sends `/authorize` command

    :param user: User object
    :type user: :class:`User`

    :param message: Telegram message
    :type message: :class:`types.Message`

    :return: Sends a message
    """
    await message.answer(
        _(
            'Follow this link: '
        ) + (
            'https://accounts.spotify.com/authorize'
            '?response_code=code'
            '&client_id={client_id}'
            '&scope={scopes}'
            '&redirect_uri={redirect_uri}'
        ).format(
            client_id=Config.SPOTIFY_CLIENT_ID,
            scopes=','.join(Config.SPOTIFY_SCOPES),
            redirect_uri='test',
        ),
        disable_web_page_preview=True
    )
