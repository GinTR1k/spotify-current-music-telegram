import logging

from aiogram import types

from spotify_current_music_telegram.models import User
from spotify_current_music_telegram.telegram import dispatcher
from spotify_current_music_telegram.telegram.locale import _
from spotify_current_music_telegram.utils import auth_not_required


@dispatcher.message_handler(commands=['agree_terms'])
@auth_not_required()
async def agree_terms(message: types.Message, user: User):
    """This handler will be called when user sends `/agree_terms` command

    :param user: User object
    :type user: :class:`User`

    :param message: Telegram message
    :type message: :class:`types.Message`

    :return: Sends a message
    """
    if user:
        return await message.answer(_(
            'You have already agreed terms of use.\n'
            'Everything is OK =)'
        ))

    try:
        await User.create(telegram_user_id=message.from_user.id)
    except Exception as e:
        logging.error(e, exc_info=e)
        return await message.reply(
            _('Hey. Something went wrong. Our developers already noticed.'))

    await message.answer(_(
        'Now, you should to authorize in Spotify to use this bot '
        'functionality. Send /authorize command.'))
