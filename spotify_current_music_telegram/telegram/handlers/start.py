from aiogram import types

from spotify_current_music_telegram.models import User
from spotify_current_music_telegram.telegram import dispatcher
from spotify_current_music_telegram.telegram.locale import _
from spotify_current_music_telegram.utils import auth_not_required


@dispatcher.message_handler(commands=['start', 'help'])
@auth_not_required()
async def send_welcome(message: types.Message, user: User):
    """This handler will be called when user sends `/start` or `/help` command

    :param user: User object
    :type user: :class:`User`

    :param message: Telegram message
    :type message: :class:`types.Message`

    :return: Sends a message
    """
    message_text = _(
        'Hello!\n'
        'I\'m a bot which shows your current song which you listening for '
        'in [Spotify](https://spotify.com).\n'
    )

    if not user:
        message_text += _(
            '\nTo continuing using this bot, you must agree with following '
            'terms of use:\n'
            '1. This bot will collect and store your Spotify and Telegram '
            'information -- user\_id;\n'
            '2. For showing personal Spotify status we need to authorize '
            'you in Spotify;\n'
            '3. The software is provided "as is", without warranty of any '
            'kind.\n\n'
            'If you agree, send command /agree\_terms'
        )

    await message.answer(message_text, parse_mode='Markdown',
                         disable_web_page_preview=True)
