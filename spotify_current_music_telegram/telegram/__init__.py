from aiogram import Bot, Dispatcher
from aiogram.bot import api
from spotify_current_music_telegram.telegram.locale import i18n

from spotify_current_music_telegram.config import Config
from spotify_current_music_telegram.telegram.middlewares import \
    GetUserMiddleware


# Set up API URL
if Config.TELEGRAM_BOT_API_PROXY:
    api.API_URL = Config.TELEGRAM_BOT_API_PROXY + "/bot{token}/{method}"

bot = Bot(token=Config.TELEGRAM_BOT_API_TOKEN)
dispatcher = Dispatcher(bot)

# locale
dispatcher.middleware.setup(i18n)
# User in data
dispatcher.middleware.setup(GetUserMiddleware())

# Import handlers
from spotify_current_music_telegram.telegram.handlers import (
    start, agree_terms, authorize_spotify
)
