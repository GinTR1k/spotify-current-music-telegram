def auth_not_required():
    def wrapper(func):
        setattr(func, 'auth_not_required', True)
        return func
    return wrapper
