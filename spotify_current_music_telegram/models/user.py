from datetime import datetime, timedelta

from spotify_current_music_telegram.models import db
from spotify_current_music_telegram.utils import generate_uuid


class User(db.Model):
    __tablename__ = 'users'

    uuid = db.Column(db.String(40), primary_key=True, default=generate_uuid)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    telegram_user_id = db.Column(db.Integer, index=True, unique=True)
    telegram_authorized_at = db.Column(db.DateTime, default=datetime.utcnow)
    spotify_token = db.Column(db.String)
    spotify_authorized_at = db.Column(db.DateTime)
    spotify_expires_date = db.Column(db.DateTime)

    @property
    def is_spotify_authorized(self) -> bool:
        return (
            datetime.utcnow() > self.spotify_expires_date
            if self.spotify_token else False
        )

    def set_spotify_authorization(self, token: str, expires_in: int):
        utc_now = datetime.utcnow()

        self.spotify_token = token
        self.spotify_authorized_at = utc_now
        self.spotify_expires_date = utc_now - timedelta(seconds=expires_in)
