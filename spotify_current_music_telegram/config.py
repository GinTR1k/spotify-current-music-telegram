from environs import Env


env = Env()
env.read_env()


class Config:
    DEBUG: bool = env.bool('DEBUG')
    # Database
    DATABASE_URI = env.str('DATABASE_URI')
    SQLALCHEMY_DATABASE_DIALECT = env.str('SQLALCHEMY_DATABASE_DIALECT', None)

    # Spotify
    SPOTIFY_CLIENT_ID: str = env.str('SPOTIFY_CLIENT_ID')
    SPOTIFY_CLIENT_SECRET: str = env.str('SPOTIFY_CLIENT_SECRET')
    SPOTIFY_SCOPES: tuple = ('user-read-currently-playing',)

    # Telegram
    TELEGRAM_BOT_API_TOKEN: str = env.str('TELEGRAM_BOT_API_TOKEN')
    # API endpoint, optional
    # Uses when telegram is blocked in your country
    TELEGRAM_BOT_API_PROXY: str = env.str('TELEGRAM_BOT_API_PROXY', None)
