import logging

from aiogram.utils import executor

from spotify_current_music_telegram.models import db
from spotify_current_music_telegram.telegram import dispatcher
from spotify_current_music_telegram.config import Config


async def initialize_db(*args, **kwargs):
    await db.set_bind(Config.DATABASE_URI)


def run():
    if Config.DEBUG:
        logging.basicConfig(level=logging.DEBUG)

    executor.start_polling(dispatcher, skip_updates=True, on_startup=initialize_db)


if __name__ == '__main__':
    run()
