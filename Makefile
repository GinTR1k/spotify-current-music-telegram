run_telegram_bot:
	python app.py

translation_extract:
	pybabel extract -F babel.cfg -o locales/messages.pot .

translation_update:
	pybabel update -i locales/messages.pot -d locales

translation_compile:
	pybabel compile -d locales
